## Installation

- Clone repo

- Install Virtualbox, Ansible and Vagrant on host machine

- Run `vagrant up` in the `CentOS_7_Vagrant` folder to install, start and provision the VMs

## Managing the VMs with Vagrant

- To start up all VMs run `vagrant up` in the folder with the Vagrantfile. You can also only launch specific VMs by specifying their name with `vagrant up <vm name>`

- To shut down: `vagrant halt` or `vagrant halt <vm name>`

- To delete the VMs: `vagrant destroy` or `vagrant destroy <vm name>`

- To access the VMs: `vagrant ssh <vm name>`

- To move files to the VMs we use rsync: `vagrant rsync` or `vagrant rsync <vm name>`

Replace <vm name> with one of the following: client, primaryserver, dataserver1, dataserver2, computeserver1, computeserver2

The virtual machines are provided by Vagrant, they are generic/centos7 type boxes version 3.4.2


## Accessing the DIRAC WebGUI of the primary server

Add the certificate `browser certificate.p12` to your web browser and open the URL https://localhost:8443

## Accessing the DIRAC Configuration system

- Run `vagrant ssh client` to connect to the Client VM

- Run `dirac-proxy-init -g dirac_admin -U` to get admin access to the DIRAC servers

- to open the DIRAC server configuration system on the primary server: `dirac-admin-sysadmin-cli  --host dirac-tuto`

- to open the DIRAC server configuration system on the data servers: `dirac-admin-sysadmin-cli  --host dataserver1`

## Editing the DIRAC configuration

The configuration file for a DIRAC machine is located in `/CentOS 7 Vagrant/<servername>` folder in dirac.cfg

After DIRAC is installed on the server, it is located in /opt/dirac/etc/dirac.cfg

![](Systemarchitektur.svg)
