\babel@toc {german}{}
\contentsline {title}{Expose: Test der AMD ROCm Open-Source-Softwareplattform auf lokalen Radeon Instinct-GPUs.}{1}{chapter.1}%
\authcount {1}
\contentsline {author}{László Erdős\unskip \ \ignorespaces {\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ {\leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ }\belowdisplayskip \abovedisplayskip Referenten: Dr. Kilian Schwarz, Dr. Ralf Mayer}}{1}{chapter.1}%
\contentsline {section}{\numberline {1}Stand der Forschung}{3}{section.1.1}%
\contentsline {section}{\numberline {2}Singularity-Container}{4}{section.1.2}%
\contentsline {section}{\numberline {3}Programmierung von Grafikkarten}{4}{section.1.3}%
\contentsline {section}{\numberline {4}ROCm}{4}{section.1.4}%
\contentsline {section}{\numberline {5}Benchmarks}{4}{section.1.5}%
\contentsline {section}{\numberline {6}Performance: CPU vs HIP}{4}{section.1.6}%
\contentsline {section}{\numberline {7}Performance: Vergleich ROCm mit CUDA}{4}{section.1.7}%
\contentsline {section}{\numberline {8}CUDA zu HIP konvertieren mit HIPIFY}{4}{section.1.8}%
