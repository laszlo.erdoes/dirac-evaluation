\babel@toc {german}{}
\contentsline {title}{Praktikumsbericht}{1}{chapter.1}%
\authcount {1}
\contentsline {author}{László Erdős\unskip \ \ignorespaces {\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ {\leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ }\belowdisplayskip \abovedisplayskip Referent: Dr. Kilian Schwarz} \unskip \ \ignorespaces {\relax \fontsize {9}{11}\selectfont \abovedisplayskip 8.5\p@ plus3\p@ minus4\p@ \abovedisplayshortskip \z@ plus2\p@ \belowdisplayshortskip 4\p@ plus2\p@ minus2\p@ \def \leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ {\leftmargin \leftmargini \parsep 0\p@ plus1\p@ minus\p@ \topsep 8\p@ plus2\p@ minus4\p@ \itemsep 0\p@ }\belowdisplayskip \abovedisplayskip Betreuer: Raffaele Grosso, Dr. Kilian Schwarz}}{1}{chapter.1}%
\contentsline {section}{\numberline {1}Eigenständigkeitserklärung}{3}{section.1.1}%
\contentsline {section}{\numberline {2}Einleitung, Projektübersicht}{4}{section.1.2}%
\contentsline {section}{\numberline {3}Das Unternehmen}{4}{section.1.3}%
\contentsline {section}{\numberline {4}Vagrant}{5}{section.1.4}%
\contentsline {section}{\numberline {5}Ansible}{5}{section.1.5}%
\contentsline {section}{\numberline {6}DIRAC}{5}{section.1.6}%
\contentsline {section}{\numberline {7}DIRAC-Systemübersicht}{6}{section.1.7}%
\contentsline {subsection}{\numberline {7.1}Prinzipien}{6}{subsection.1.7.1}%
\contentsline {subsection}{\numberline {7.2}Systembestandteile}{6}{subsection.1.7.2}%
\contentsline {section}{\numberline {8}Das virtuelle DIRAC-Testcluster}{6}{section.1.8}%
